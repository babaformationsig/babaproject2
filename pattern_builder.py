import os
from typing import Callable, Dict, Tuple, Optional
from abc import ABC, abstractmethod
import django

os.environ["DJANGO_SETTINGS_MODULE"] = "efficity.settings"
django.setup()

from efficity.formflow.lib.form_manager import FormManager
from efficity.formflow.lib.formflow import FormFlow, FormNode
from efficity.formflow.discover_forms import ALIAS_TO_FORMS


"""
    This is the actual structure of the formflow
    To render simple the creation of the formflow, we can use the Builder Pattern
"""


def actual_structure_formflow():
    create_person_and_house = FormFlow("create_person_and_house", CREATE_BIEN_TXT)
    email_form = create_person_and_house.add_node(ALIAS_TO_FORMS["PersonEmailCreateHouse"], title=CREATE_PERSON_TXT)
    account_form = create_person_and_house.add_node(ALIAS_TO_FORMS["CreatePersonForm"], title=CREATE_PERSON_TXT)
    house_type = create_person_and_house.add_node(ALIAS_TO_FORMS["HouseTypeLight"], title="Choix du type de bien")
    home_light = create_person_and_house.add_node(ALIAS_TO_FORMS["HomeLight"], title="Fiche maison partie 1")
    home_light2 = create_person_and_house.add_node(ALIAS_TO_FORMS["HomeLight2"], title="Fiche maison partie 2")
    home_light3 = create_person_and_house.add_node(ALIAS_TO_FORMS["HomeLight3"], title="Fiche maison partie 3")
    owner_exists = create_person_and_house.add_node(ALIAS_TO_FORMS["OwnerExistsForm"], title="Propriétaire existe?")
    flat_light = create_person_and_house.add_node(ALIAS_TO_FORMS["FlatLight"], title="Fiche appartement partie 1")
    flat_light2 = create_person_and_house.add_node(ALIAS_TO_FORMS["FlatLight2"], title="Fiche appartement partie 2")
    without_surface_helper_text = create_person_and_house.add_node(
        ALIAS_TO_FORMS["WithoutSurfaceHelperText"], title=CREATE_FICHE_BIEN_TXT
    )
    other_than_home_light = create_person_and_house.add_node(
        ALIAS_TO_FORMS["OtherThanHomeLight"], title=CREATE_FICHE_BIEN_TXT
    )
    seller_estim = create_person_and_house.add_node(
        ALIAS_TO_FORMS["SellerEstimationHouseLight"],
        title=CREATE_FICHE_PROJET_VENDEUR_TXT,
    )
    flat_seller_estim = create_person_and_house.add_node(
        ALIAS_TO_FORMS["SellerEstimationFlatLight"],
        title=CREATE_FICHE_PROJET_VENDEUR_TXT,
    )
    other_seller_estim = create_person_and_house.add_node(
        ALIAS_TO_FORMS["SellerEstimationOtherLight"],
        title=CREATE_FICHE_PROJET_VENDEUR_TXT,
    )
    create_person_and_house.add_edge(email_form, account_form, "email_already_has_person==False")
    create_person_and_house.add_edge(email_form, house_type, "email_already_has_person==True")
    create_person_and_house.add_edge(account_form, house_type, "True")
    create_person_and_house.add_edge(house_type, home_light, "int(house_type)==2")
    create_person_and_house.add_edge(home_light, home_light2, "True")
    create_person_and_house.add_edge(home_light2, home_light3, "True")
    create_person_and_house.add_edge(home_light3, owner_exists, "True")
    create_person_and_house.add_edge(flat_light, flat_light2, "True")
    create_person_and_house.add_edge(flat_light2, owner_exists, "True")
    create_person_and_house.add_edge(without_surface_helper_text, owner_exists, "True")
    create_person_and_house.add_edge(other_than_home_light, owner_exists, "True")
    create_person_and_house.add_edge(house_type, flat_light, "int(house_type)==1")
    create_person_and_house.add_edge(
        house_type,
        other_than_home_light,
        "int(house_type)!=2 and int(house_type)!=1 and int(house_type)!=7 and int(house_type)!=9",
    )
    create_person_and_house.add_edge(
        house_type, without_surface_helper_text, "int(house_type)==7 or int(house_type)==9"
    )
    create_person_and_house.add_edge(owner_exists, seller_estim, "int(house_type)==2 and owner_exists in [1,2]")
    create_person_and_house.add_edge(owner_exists, flat_seller_estim, "int(house_type)==1 and owner_exists in [1,2]")
    create_person_and_house.add_edge(
        owner_exists,
        other_seller_estim,
        "int(house_type)!=1 and int(house_type)!=2 and owner_exists in [1,2]",
    )

    print("Old FormFlow:")
    print(create_person_and_house.__dict__)
    print("--------------------------------------")


"""
    This is the structure of the Builder Pattern
        1. Product: Define the product, which represents a form field with a label and input type.
        2. Builder (Abstract Builder): Create an abstract builder interface with methods for adding form fields and building the form.
        3. Concrete Builder: Implement a concrete builder that adds specific form fields and builds the form.
        4. Director: Implement a director responsible for orchestrating the construction of the form using the builder.
"""

NodeMappingType = Dict[str, str]
EdgeMappingType = Tuple[Tuple[str, str, str], ...]
FormFlowModelType = Callable[[str, str], FormFlow]


# Step 1: Define the Product
class FormFlowInit:
    @classmethod
    def create_form_flow_init(
        cls,
        name_form_flow: str,
        label_name_formflow: str,
        form_flow_model: FormFlowModelType,
        formflow_extractor: Optional[Callable[..., None]] = None,
    ) -> FormFlow:
        form_flow = form_flow_model(name_form_flow, label_name_formflow)
        form_flow.label = label_name_formflow  # type: ignore
        form_flow.description = label_name_formflow  # type: ignore
        form_flow.extractor = formflow_extractor  # type: ignore
        return form_flow


# Step 2: Define the Builder (Abstract Builder)
class IFormFlowBuilder(ABC):
    @abstractmethod
    def get_node(self, alias: str) -> FormNode: ...

    @abstractmethod
    def build_nodes_to_formflow(self, node_mapping: NodeMappingType, alias_forms: FormManager) -> None: ...

    @abstractmethod
    def build_edges_to_formflow(self, edge_mapping: EdgeMappingType) -> None: ...


# Step 3: Implement the Concrete Builder
class FormFlowBuilder(IFormFlowBuilder):
    def __init__(self, form_flow_model: FormFlow):
        self.form_flow = form_flow_model

    def get_node(self, alias: str) -> FormNode:  # type: ignore
        for node in self.form_flow.nodes:  # type: ignore
            if node.frm().__class__.__name__ == alias:
                return node  # type: ignore

    def build_nodes_to_formflow(self, node_mapping: NodeMappingType, alias_forms: FormManager) -> None:
        for alias, title in node_mapping.items():
            _ = self.form_flow.add_node(alias_forms[alias], title=title)

    def build_edges_to_formflow(self, edge_mapping: EdgeMappingType) -> None:
        for from_node, to_node, condition in edge_mapping:
            self.form_flow.add_edge(self.get_node(from_node), self.get_node(to_node), condition)


# Step 4: Implement the Director
class FormBuilderDirector:
    """Director for constructing complex forms using a specific builder."""

    def __init__(
        self,
        builder: IFormFlowBuilder,
        node_mapping_build: NodeMappingType,
        edge_mapping_build: EdgeMappingType,
        alias_forms: FormManager,
    ):
        self.builder = builder
        self.node_mapping_build = node_mapping_build
        self.edge_mapping_build = edge_mapping_build
        self.alias_forms = alias_forms

    def construct_formflow(self):
        """Construct the form using the builder."""
        self.builder.build_nodes_to_formflow(self.node_mapping_build, self.alias_forms)
        self.builder.build_edges_to_formflow(self.edge_mapping_build)


# Step 5: Implement the Client
if __name__ == "__main__":
    # # Suscribe all Form needed in FormManager before using it
    #     # Methode Module
    #         ALIAS_TO_FORMS.discoverForms("NameModule")
    #     # Methode Form File
    #         ALIAS_TO_FORMS.add("NameForm", category="Category")

    # Declare All Forms Name
    PERSON_EMAIL_CREATE_HOUSE = "PersonEmailCreateHouse"
    CREATE_PERSON_FORM = "CreatePersonForm"
    HOUSE_TYPE_LIGHT = "HouseTypeLight"
    HOME_LIGHT = "HomeLight"
    HOME_LIGHT2 = "HomeLight2"
    HOME_LIGHT3 = "HomeLight3"
    OWNER_EXISTS_FORM = "OwnerExistsForm"
    FLAT_LIGHT = "FlatLight"
    FLAT_LIGHT2 = "FlatLight2"
    WITHOUT_SURFACE_HELPER_TEXT = "WithoutSurfaceHelperText"
    OTHER_THAN_HOME_LIGHT = "OtherThanHomeLight"
    SELLER_ESTIMATION_HOUSE_LIGHT = "SellerEstimationHouseLight"
    SELLER_ESTIMATION_FLAT_LIGHT = "SellerEstimationFlatLight"
    SELLER_ESTIMATION_OTHER_LIGHT = "SellerEstimationOtherLight"

    # Declare Node Mapping With Name Title
    CREATE_PERSON_TXT = "Création d’une personne"
    CREATE_BIEN_TXT = "Créer un bien"
    CREATE_FICHE_BIEN_TXT = "Fiche bien"
    CREATE_FICHE_PROJET_VENDEUR_TXT = "Fiche projet Vendeur"
    node_mapping = {
        PERSON_EMAIL_CREATE_HOUSE: CREATE_PERSON_TXT,
        CREATE_PERSON_FORM: CREATE_PERSON_TXT,
        HOUSE_TYPE_LIGHT: "Choix du type de bien",
        HOME_LIGHT: "Fiche maison partie 1",
        HOME_LIGHT2: "Fiche maison partie 2",
        HOME_LIGHT3: "Fiche maison partie 3",
        OWNER_EXISTS_FORM: "Propriétaire existe?",
        FLAT_LIGHT: "Fiche appartement partie 1",
        FLAT_LIGHT2: "Fiche appartement partie 2",
        WITHOUT_SURFACE_HELPER_TEXT: CREATE_FICHE_BIEN_TXT,
        OTHER_THAN_HOME_LIGHT: CREATE_FICHE_BIEN_TXT,
        SELLER_ESTIMATION_HOUSE_LIGHT: CREATE_FICHE_PROJET_VENDEUR_TXT,
        SELLER_ESTIMATION_FLAT_LIGHT: CREATE_FICHE_PROJET_VENDEUR_TXT,
        SELLER_ESTIMATION_OTHER_LIGHT: CREATE_FICHE_PROJET_VENDEUR_TXT,
    }

    # Declare Edge Mapping nested with Condition
    edge_mapping = (
        (PERSON_EMAIL_CREATE_HOUSE, CREATE_PERSON_FORM, "email_already_has_person==False"),
        (PERSON_EMAIL_CREATE_HOUSE, HOUSE_TYPE_LIGHT, "email_already_has_person==True"),
        (CREATE_PERSON_FORM, HOUSE_TYPE_LIGHT, "True"),
        (HOUSE_TYPE_LIGHT, HOME_LIGHT, "int(house_type)==2"),
        (HOME_LIGHT, HOME_LIGHT2, "True"),
        (HOME_LIGHT2, HOME_LIGHT3, "True"),
        (HOME_LIGHT3, OWNER_EXISTS_FORM, "True"),
        (FLAT_LIGHT, FLAT_LIGHT2, "True"),
        (FLAT_LIGHT2, OWNER_EXISTS_FORM, "True"),
        (WITHOUT_SURFACE_HELPER_TEXT, OWNER_EXISTS_FORM, "True"),
        (OTHER_THAN_HOME_LIGHT, OWNER_EXISTS_FORM, "True"),
        (HOUSE_TYPE_LIGHT, FLAT_LIGHT, "int(house_type)==1"),
        (
            HOUSE_TYPE_LIGHT,
            OTHER_THAN_HOME_LIGHT,
            "int(house_type)!=2 and int(house_type)!=1 and int(house_type)!=7 and int(house_type)!=9",
        ),
        (HOUSE_TYPE_LIGHT, WITHOUT_SURFACE_HELPER_TEXT, "int(house_type)==7 or int(house_type)==9"),
        (OWNER_EXISTS_FORM, SELLER_ESTIMATION_HOUSE_LIGHT, "int(house_type)==2 and owner_exists in [1,2]"),
        (OWNER_EXISTS_FORM, SELLER_ESTIMATION_FLAT_LIGHT, "int(house_type)==1 and owner_exists in [1,2]"),
        (
            OWNER_EXISTS_FORM,
            SELLER_ESTIMATION_OTHER_LIGHT,
            "int(house_type)!=1 and int(house_type)!=2 and owner_exists in [1,2]",
        ),
    )

    # Demonstrate formflow generation using the Builder Pattern
    base_form_flow = FormFlowInit.create_form_flow_init("create_person_and_house", CREATE_BIEN_TXT, FormFlow)
    builder = FormFlowBuilder(base_form_flow)
    director = FormBuilderDirector(builder, node_mapping, edge_mapping, ALIAS_TO_FORMS)
    director.construct_formflow()

    print("Generated Form:")
    print(base_form_flow.__dict__)
    print("--------------------------------------")

    # To compare Result off builder Pattern and actual structure create formflow
    actual_structure_formflow()